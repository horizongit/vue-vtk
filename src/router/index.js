import { createRouter, createWebHistory } from 'vue-router'
    import Test from '../components/Test.vue'
    import Test2 from '../components/Test2.vue'
    import Test3 from '../components/Test3.vue'
    import Test4 from '../components/Test4.vue'
    import Test5 from '../components/Test5.vue'
    import Test6 from '../components/Test6.vue'
    const routes = [
      { path: '/test', component: Test},
      { path: '/test2', component: Test2},
      { path: '/test3', component: Test3},
      { path: '/test4', component: Test4},
      { path: '/test5', component: Test5},
      { path: '/test6', component: Test6},
    ]
    const router = createRouter({
      history: createWebHistory(process.env.BASE_URL),
      routes
    })
    export default router